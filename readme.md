# Advanced Search Sample

This project is just a simple sample that shows a way to create an advanced search feature. It was made to solve these specific problems:

  - Different Types of Search Criteria
    - Radio Buttons
    - Checkboxes
    - Dropdowns
    - Number Ranges
    - Date Ranges
    - etc
- Multiple Tables
- Multiple Columns
- Performance Concerns
- Flexible to Easily Add Additional Criteria
- Needed to be Maintained by Junior Developers

### Installation

1. Run the CreateDatabase.sql in the db folder
2. Run PopulateDatabase.sql in the db folder
3. Open the Solution
4. Check the ConnectionString to make sure the path matches your own
5. Run it!


