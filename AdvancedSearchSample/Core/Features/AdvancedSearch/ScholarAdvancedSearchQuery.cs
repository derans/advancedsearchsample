using System.Collections.Generic;
using NPoco;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch
{
    public class ScholarAdvancedSearchQuery
    {
        private readonly ISearchCriteriaFactory _searchCriteriaFactory;

        public ScholarAdvancedSearchQuery(ISearchCriteriaFactory searchCriteriaFactory)
        {
            _searchCriteriaFactory = searchCriteriaFactory;
        }

        protected const string SqlSearch =
            @"SELECT  * FROM (SELECT DISTINCT
                                  Scholar.[ScholarId]
                                , Scholar.[LastName]
                                , Scholar.[FirstName]
                                , Scholar.[Gender]
                                , Scholar.[ScholarYear] AS 'Year'
                                , Scholar.[Hometown]
                                {0}
                                FROM [Scholar]
                                {1}) 
            as t {2} ORDER BY LastName, FirstName";


        public IList<ScholarAdvancedSearchResultLineItem> Execute(IScholarSearchCriteria criteria, bool requireAllTableDependencies = false)
        {
            var filter = Filter.CreateFilter(criteria, _searchCriteriaFactory.GetSearchCriteria(), requireAllTableDependencies);

            using (var db = new Database("AdvancedSearch"))
            {
                var sql = string.Format(SqlSearch, filter.AdditionalColumns, filter.JoinClause, filter.WhereClause);
                return db.Fetch<ScholarAdvancedSearchResultLineItem>(sql, filter.Parameters.ToArray());
            }
        }
    }
}
