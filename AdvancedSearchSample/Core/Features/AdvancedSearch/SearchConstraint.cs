using System.Collections.Generic;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch
{
    public class SearchConstraint
    {
        public string Clause { get; set; }
        public IEnumerable<ISqlTableDependency> SqlTableDependencies { get; set; }
        public IEnumerable<object> Parameters { get; set; }
    }
}
