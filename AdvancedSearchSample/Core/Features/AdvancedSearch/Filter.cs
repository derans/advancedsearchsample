using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch
{
    public class Filter
    {
        public static Filter CreateFilter(IScholarSearchCriteria criteria, IEnumerable<IScholarSearchCriteriaAppender> searchCriteriaList, bool requireAllTableDependencies)
        {
            var filter = new Filter();
            var cnt = 0;
            foreach (var searchCriteria in searchCriteriaList.Where(x => x.ShouldApply(criteria)))
            {
                var constraint = searchCriteria.GenerateConstraint(criteria, cnt);

                filter.AddCondition(constraint.Clause);
                filter.AddTableDependencies(constraint.SqlTableDependencies);
                filter.AddParameters(constraint.Parameters);
                cnt += constraint.Parameters.Count();
            }

            if (requireAllTableDependencies)
                filter.AddAllTableDependencies();

            return filter;
        }

        private void AddAllTableDependencies()
        {
            var type = typeof(ISqlTableDependency);

            var assembly = Assembly.GetAssembly(type);
            var appenderTypes = assembly.GetTypes()
                                        .Where(tableDependency => type.IsAssignableFrom(tableDependency) && tableDependency.IsClass && !tableDependency.IsAbstract);

            AddTableDependencies(appenderTypes.Select(Activator.CreateInstance).Cast<ISqlTableDependency>());
        }

        private void AddTableDependencies(IEnumerable<ISqlTableDependency> sqlTableDependencies)
        {
            if (sqlTableDependencies == null)
                return;

            foreach (var dependency in sqlTableDependencies.Where(dependency => !TableDependencies.Contains(dependency)))
            {
                TableDependencies.Add(dependency);
                AddParameters(dependency.Parameters);
            }
        }

        private void AddParameters(IEnumerable<object> parameters)
        {
            if (parameters != null)
                Parameters.AddRange(parameters);
        }

        private void AddCondition(string condition)
        {
            if (string.IsNullOrEmpty(condition))
                return;

            if (ConditionList.All(x => x.ToUpper() != condition.ToUpper()))
                ConditionList.Add(condition);
        }

        private List<string> ConditionList { get; set; }

        private List<ISqlTableDependency> TableDependencies { get; set; }

        public string JoinClause
        {
            get
            {
                if (TableDependencies.Any())
                {
                    return string.Join(" ", TableDependencies.Select(x => x.JoinClause).Distinct());
                }
                return "";
            }
        }

        public string AdditionalColumns
        {
            get
            {
                if (TableDependencies.Any())
                {
                    return "," + string.Join(",", TableDependencies.SelectMany(x => x.AssociatedColumns).Distinct());
                }
                return "";
            }
        }

        public string WhereClause
        {
            get
            {
                if (ConditionList.Any())
                {
                    return "WHERE (" + string.Join(") AND (", ConditionList) + ")";
                }
                return "";
            }
        }

        public List<object> Parameters { get; private set; }

        private Filter()
        {
            TableDependencies = new List<ISqlTableDependency>();
            ConditionList = new List<string>();
            Parameters = new List<object>();
        }
    }
}
