namespace AdvancedSearchSample.Core.Features.AdvancedSearch
{
    public class ScholarAdvancedSearchResultLineItem
    {
        public int ScholarId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Year { get; set; }
        public decimal? GPA { get; set; }
        public string Hometown { get; set; }
    }
}
