using System.Collections.Generic;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch
{
    public class AdvancedSearchViewModel : IScholarSearchCriteria
    {
        public AdvancedSearchViewModel()
        {
            Result = new List<ScholarAdvancedSearchResultLineItem>();
        }

        public string Hometown { get; set; }
        public int? ScholarYear { get; set; }
        public string Gender { get; set; }
        public double? MinGPA { get; set; }
        public double? MaxGPA { get; set; }
        public IList<ScholarAdvancedSearchResultLineItem> Result { get; set; }
        public string LastName { get; set; }
    }
}
