using System.Collections.Generic;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch.TableDependencies
{
    public class AcademicTableDependency : BaseSqlTableDependency
    {
        protected override List<ColumnDefinition> ColumnDefinitions => new List<ColumnDefinition>
        {
            new ColumnDefinition("ScholarGPA.GPA", "GPA"),
        };

        public override string JoinClause => @"LEFT OUTER JOIN ScholarGPA ON Scholar.ScholarID = ScholarGPA.ScholarID";
    }
}
