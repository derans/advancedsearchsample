using System.Collections.Generic;
using System.Linq;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch.TableDependencies
{
    public abstract class BaseSqlTableDependency : ISqlTableDependency
    {
        protected class ColumnDefinition
        {
            public ColumnDefinition(string definition, string alias)
            {
                Alias = alias;
                Definition = definition;
            }

            public string Alias { get; set; }
            public string Definition { get; set; }
        }

        public IEnumerable<string> AssociatedColumns
        {
            get
            {
                return ColumnDefinitions.Select(x => x.Definition + " AS " + x.Alias).ToList();
            }
        }

        protected abstract List<ColumnDefinition> ColumnDefinitions { get; }
        public abstract string JoinClause { get; }
        public virtual IEnumerable<object> Parameters => null;
    }
}
