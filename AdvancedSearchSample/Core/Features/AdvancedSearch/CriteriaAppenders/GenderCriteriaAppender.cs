﻿using System.Collections.Generic;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch.CriteriaAppenders
{
    public class GenderCriteriaAppender : IScholarSearchCriteriaAppender
    {
        public SearchConstraint GenerateConstraint(IScholarSearchCriteria criteria, int parameterNumber)
        {
            var parameters = new List<string>
            {
                criteria.Gender
            };

            return new SearchConstraint
            {
                Clause = $"Gender = @{parameterNumber}",
                Parameters = parameters,
            };
        }

        public bool ShouldApply(IScholarSearchCriteria criteria)
        {
            return criteria.Gender == "Male" || criteria.Gender == "Female";
        }
    }
}
