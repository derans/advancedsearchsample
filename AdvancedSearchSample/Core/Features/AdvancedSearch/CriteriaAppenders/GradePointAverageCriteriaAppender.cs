using System.Collections.Generic;
using AdvancedSearchSample.Core.Features.AdvancedSearch.TableDependencies;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch.CriteriaAppenders
{
    public class GradePointAverageCriteriaAppender : IScholarSearchCriteriaAppender
    {
        public SearchConstraint GenerateConstraint(IScholarSearchCriteria criteria, int parameterNumber)
        {
            var parameters = new List<object>
            {
                criteria.MinGPA,
                criteria.MaxGPA,
            };

            return new SearchConstraint
            {
                Clause = $"GPA >= @{parameterNumber} AND GPA <= @{parameterNumber+1}",
                Parameters = parameters,
                SqlTableDependencies = new[] { new AcademicTableDependency() },
            };
        }

        public bool ShouldApply(IScholarSearchCriteria criteria)
        {
            return criteria.MinGPA != null || criteria.MaxGPA != null;
        }
    }
}
