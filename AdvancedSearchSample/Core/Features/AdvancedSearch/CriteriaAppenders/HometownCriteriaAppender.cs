﻿using System.Collections.Generic;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch.CriteriaAppenders
{
    public class HometownCriteriaAppender : IScholarSearchCriteriaAppender
    {
        public SearchConstraint GenerateConstraint(IScholarSearchCriteria criteria, int parameterNumber)
        {
            var parameters = new List<object>
            {
                criteria.Hometown,
            };

            return new SearchConstraint
            {
                Clause = $"Hometown = @{parameterNumber}",
                Parameters = parameters,
            };
        }

        public bool ShouldApply(IScholarSearchCriteria criteria)
        {
            return criteria.Hometown != null;
        }
    }
}
