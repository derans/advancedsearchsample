using System.Collections.Generic;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch.CriteriaAppenders
{
    public class ScholarYearCriteriaAppender : IScholarSearchCriteriaAppender
    {
        public SearchConstraint GenerateConstraint(IScholarSearchCriteria criteria, int parameterNumber)
        {
            var parameters = new List<object>
            {
                criteria.ScholarYear,
            };

            return new SearchConstraint
            {
                Clause = $"Year = @{parameterNumber}",
                Parameters = parameters,
            };
        }

        public bool ShouldApply(IScholarSearchCriteria criteria)
        {
            return criteria.ScholarYear != null;
        }
    }
}
