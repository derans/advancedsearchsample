﻿using System.Collections.Generic;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch.CriteriaAppenders
{
    public class LastNameCriteriaAppender : IScholarSearchCriteriaAppender
    {
        public SearchConstraint GenerateConstraint(IScholarSearchCriteria criteria, int parameterNumber)
        {
            var parameters = new List<object>
            {
                criteria.LastName,
            };

            return new SearchConstraint
            {
                Clause = $"LastName = @{parameterNumber}",
                Parameters = parameters,
            };
        }

        public bool ShouldApply(IScholarSearchCriteria criteria)
        {
            return criteria.LastName != null;
        }
    }
}
