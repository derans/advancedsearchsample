using System.Collections.Generic;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch
{
    public interface ISqlTableDependency
    {
        string JoinClause { get; }
        IEnumerable<string> AssociatedColumns { get; }
        IEnumerable<object> Parameters { get; }
    }
}
