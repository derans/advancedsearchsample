namespace AdvancedSearchSample.Core.Features.AdvancedSearch
{
    public interface IScholarSearchCriteriaAppender
    {
        bool ShouldApply(IScholarSearchCriteria criteria);
        SearchConstraint GenerateConstraint(IScholarSearchCriteria criteria, int parameterNumber);
    }
}
