namespace AdvancedSearchSample.Core.Features.AdvancedSearch
{
    public interface IScholarSearchCriteria
    {
        string Hometown { get; set; }
        int? ScholarYear { get; set; }
        string Gender { get; set; }
        double? MinGPA { get; set; }
        double? MaxGPA { get; set; }
        string LastName { get; set; }
    }
}
