using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AdvancedSearchSample.Core.Features.AdvancedSearch
{
    public interface ISearchCriteriaFactory
    {
        IList<IScholarSearchCriteriaAppender> GetSearchCriteria();
    }

    public class SearchCriteriaFactory : ISearchCriteriaFactory
    {
        public IList<IScholarSearchCriteriaAppender> GetSearchCriteria()
        {
            var type = typeof(IScholarSearchCriteriaAppender);

            var assembly = Assembly.GetAssembly(type);
            var appenderTypes = assembly.GetTypes().Where(appenderType => type.IsAssignableFrom(appenderType) && appenderType.IsClass);

            return appenderTypes.Select(Activator.CreateInstance)
                .Cast<IScholarSearchCriteriaAppender>()
                .ToList();
        }
    }
}
