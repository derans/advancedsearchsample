﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace AdvancedSearchSample.Core
{
    public static class DummyData
    {
        public static IEnumerable<SelectListItem> ScholarYearSelectList => new List<SelectListItem>
        {
            new SelectListItem {Text = ""},
            new SelectListItem {Text = "2013"},
            new SelectListItem {Text = "2014"},
            new SelectListItem {Text = "2016"},
            new SelectListItem {Text = "2015"},
        };
    }
}
