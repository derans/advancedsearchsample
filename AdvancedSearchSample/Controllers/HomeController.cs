﻿using System.Web.Mvc;
using AdvancedSearchSample.Core.Features.AdvancedSearch;

namespace AdvancedSearchSample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(new AdvancedSearchViewModel());
        }

        [HttpPost]
        public ActionResult Index(AdvancedSearchViewModel viewModel)
        {
            var query = new ScholarAdvancedSearchQuery(new SearchCriteriaFactory());
            viewModel.Result = query.Execute(viewModel, true);

            return View(viewModel);
        }
    }
}
