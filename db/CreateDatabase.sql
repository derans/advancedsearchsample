USE [master]
GO

CREATE DATABASE [AdvancedSearchSample]
GO

USE [AdvancedSearchSample]
GO
/****** Object:  Table [dbo].[Scholar]    Script Date: 2/19/2016 1:06:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Scholar](
	[ScholarId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[ScholarYear] [int] NOT NULL,
	[Gender] [varchar](50) NOT NULL,
	[Hometown] [varchar](50) NULL,
 CONSTRAINT [PK_Scholar] PRIMARY KEY CLUSTERED 
(
	[ScholarId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScholarGPA]    Script Date: 2/19/2016 1:06:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScholarGPA](
	[ScholarGPAId] [int] IDENTITY(1,1) NOT NULL,
	[ScholarId] [int] NOT NULL,
	[GPA] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_ScholarGPA] PRIMARY KEY CLUSTERED 
(
	[ScholarGPAId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
