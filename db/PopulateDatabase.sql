Use AdvancedSearchSample

INSERT INTO Scholar(FirstName, LastName, ScholarYear, Gender, Hometown) VALUES('Speed', 'Racer', '2013', 'Male', 'Houston')
INSERT INTO Scholar(FirstName, LastName, ScholarYear, Gender, Hometown) VALUES('Trixie', 'Racer', '2014', 'Female', 'Dayton')
INSERT INTO Scholar(FirstName, LastName, ScholarYear, Gender, Hometown) VALUES('Pops', 'Racer', '2015', 'Male', 'Houston')
INSERT INTO Scholar(FirstName, LastName, ScholarYear, Gender, Hometown) VALUES('Racer', 'X', '2013', 'Male', 'Houston')
INSERT INTO Scholar(FirstName, LastName, ScholarYear, Gender, Hometown) VALUES('Mom', 'Racer', '2014', 'Female', 'Houston')

INSERT INTO ScholarGPA(ScholarId, GPA) VALUES('1', '2.5')
INSERT INTO ScholarGPA(ScholarId, GPA) VALUES('2', '4.0')
INSERT INTO ScholarGPA(ScholarId, GPA) VALUES('3', '1.2')
INSERT INTO ScholarGPA(ScholarId, GPA) VALUES('4', '4.0')
INSERT INTO ScholarGPA(ScholarId, GPA) VALUES('5', '3.2')